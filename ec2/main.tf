resource "aws_instance" "frontend" {
  ami           = "ami-0c95ddc49a2ac351f"
  instance_type = "t3.micro"
  subnet_id     = var.a_public_subnet_id
  vpc_security_group_ids = [aws_security_group.frontend-SG.id]
  iam_instance_profile = var.instance_profile

  root_block_device {
        volume_type           = "standard"
        volume_size           = 8
        delete_on_termination = true
    }

  tags = {
    Name = "frontend-server"
  }
}


resource "aws_instance" "backend" {
  ami           = "ami-0c95ddc49a2ac351f"
  instance_type = "t3.micro"
  key_name      = var.aws_key_pair
  subnet_id     = var.a_private_subnet_id
  vpc_security_group_ids = [aws_security_group.backend-SG.id]
  iam_instance_profile = var.instance_profile

   root_block_device {
        volume_type           = "standard"
        volume_size           = 8
        delete_on_termination = true
    }

  tags = {
    Name = "backend-server"
  }
}

# --------------------  Creating a Security Group for frontend --------------------------------

resource "aws_security_group" "frontend-SG" {

  # Name of the security Group!
  name = "frontend-sg"
  
  # VPC ID in which Security group has to be created!
  vpc_id = var.vpc_id

  # Created an inbound rule for frontserver access!
  ingress {
    description = "HTTP for frontserver"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  # Created an inbound rule for SSH
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outward Network Traffic for the frontserver
  egress {
    description = "output from frontserver"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# --------------------  Creating a Security Group for backend --------------------------------

resource "aws_security_group" "backend-SG" {

  # Name of the security Group!
  name = "backend-sg"
  vpc_id = var.vpc_id

 
  # Created an inbound rule for SSH
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.frontend-SG.id]
  }

  # Outward Network Traffic for the backendserver
  egress {
    description = "output from backendserver"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
