variable "vpc_id" {}
variable "a_private_subnet_id" {}
variable "a_public_subnet_id" {}
variable "instance_profile" {
}
variable "aws_key_pair" {
  default = "aws_key_ec2"
}
