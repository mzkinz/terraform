# Elasticache - Redis
resource "aws_elasticache_cluster" "redis" {
  cluster_id           = "cluster-redis"
  engine               = "redis"
  node_type            = "cache.t3.micro"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis7"
  port                 = 6379
  subnet_group_name    = aws_elasticache_subnet_group.redis-SG.name
  security_group_ids   = [aws_security_group.redis-security-group.id]
}


resource "aws_elasticache_subnet_group" "redis-SG" {
  name       = "my-cache-subnet"
  subnet_ids = [var.a_private_subnet_id]
}

# --------------------  Creating a Security Group for redis --------------------------------


resource "aws_security_group" "redis-security-group" {
  name = "redis-sg"
  description = "ElastiCache Redis Securtity Group"
  vpc_id = var.vpc_id

    ingress {
      from_port = "6739"
      to_port = "6739"
      protocol = "tcp"
      cidr_blocks = ["10.0.4.0/24"]
    }

    egress {
      from_port = "0"
      to_port = "0"
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    tags= {
      Name = "ElastiCache Redis SG"
    }
}
