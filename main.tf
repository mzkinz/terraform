terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}


# Configure the AWS Provider
provider "aws" {
  shared_credentials_files = ["~/.aws/credentials"]
  profile                  = "default"
  region = "eu-west-3"
}


terraform {
  backend "s3" {
    bucket = "backend-terraform-hajer"
    key    = "state/terraform.tfstate"
    region = "eu-west-3"
  }
}

# -- Modules -----------------------------------------------

module "vpc" {
  source = "./vpc"

}

module "s3" {
  source = "./s3"

}

module "iam" {
  source = "./iam"

}

module "ec2" {
  source = "./ec2"
  vpc_id = module.vpc.vpc_id
  a_public_subnet_id = module.vpc.public_subnet_id
  a_private_subnet_id = module.vpc.private_subnet_id
  instance_profile = module.iam.instance_profile


}


module "elasticache" {
  source = "./elasticache"
  vpc_id = module.vpc.vpc_id
  a_private_subnet_id=module.vpc.private_subnet_id


}

