
# -- ---------------------------------  vpc -------------------------------------
resource "aws_vpc" "my_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "my_vpc"
  }
}

# -- ---------------------------------  public_subnet -------------------------------------

resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone       = "eu-west-3a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnet"
  }
}

# -- ---------------------------------  private_subnet -------------------------------------

resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone       = "eu-west-3b"
  map_public_ip_on_launch = true

  tags = {
    Name = "private_subnet"
  }
}



# -- ---------------------------------  internet gateway -------------------------------------

resource "aws_internet_gateway" "gw" {
 vpc_id = aws_vpc.my_vpc.id
 
 tags = {
   Name = "Project VPC IG"
 }
}

# -- --------------------------------- public route table -------------------------------------

resource "aws_route_table" "public_rt" {
 vpc_id = aws_vpc.my_vpc.id
 
 route {
   cidr_block = "0.0.0.0/0"
   gateway_id = aws_internet_gateway.gw.id
 }
 
 tags = {
   Name = "Route Table for Internet Gateway"
 }
}

# -- ---------------------------------  public subnet association with public route table -------------------------------------

resource "aws_route_table_association" "public_subnet_asso" {
 route_table_id = aws_route_table.public_rt.id
 subnet_id      = aws_subnet.public_subnet.id
}

# -- -------------------------- Creating an Elastic IP for the NAT Gateway!-- -----------------------
resource "aws_eip" "Nat-Gateway-EIP" {
  domain           = "vpc"
}
# -- ---------------------------------  NAT Gateway -------------------------------------
resource "aws_nat_gateway" "NAT_GATEWAY" {
  depends_on = [
    aws_eip.Nat-Gateway-EIP
  ]

  # Allocating the Elastic IP to the NAT Gateway!
  allocation_id = aws_eip.Nat-Gateway-EIP.id
  
  # Associating it in the Public Subnet!
  subnet_id = aws_subnet.public_subnet.id
  tags = {
    Name = "Nat-Gateway"
  }
}


# --------------- Private Route Table for the Nat Gateway!------------------------------
resource "aws_route_table" "NAT-Gateway-RT" {
  depends_on = [
    aws_nat_gateway.NAT_GATEWAY
  ]

  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NAT_GATEWAY.id
  }

  tags = {
    Name = "Route Table for NAT Gateway"
  }

}

# -- ---------------------------------  private subnet association with private route table -------------------------------------

resource "aws_route_table_association" "Nat-Gateway-RT-Association" {
 route_table_id = aws_route_table.NAT-Gateway-RT.id
 subnet_id      = aws_subnet.private_subnet.id
}